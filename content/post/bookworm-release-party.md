---
title: "Debian bookworm release party by Debian India"
date: 2023-06-10T00:30:00+05:30
draft: false
author: "Debian India"
tags: ["debian", "bookworm", "release-party", "announcement"]
---

![Debian India bookworm release party poster](/images/poster-two.png)


We are very happy to announce the celebration of Debian Bookworm release, which is the 12th stable release of Debian. This will be an online party and you can join us from anywhere.


When: 10th June Saturday,  7:30 PM IST at https://12.debian.org.in

Where: Online at https://12.debian.org.in from an internet browserBigBlueButton


Events:
    * Technical Overview and changes in Debian Bookworm by Abraham Raji
    * Debian Trivia Quiz by Anupa Ann Joseph
    * What Debian lacks/need to improve by Akshay S Dinesh
    * Cheese & Wine - Bring your own food
    * Group photo
    * SuperTuxKart multi-player gaming session by Sahil Dhiman

For the people interested in joining the LAN party, please install SuperTuxKart before hand.

We would also like to extend our thanks to [Abhas Abhinav](https://abhas.io/) for online BigBlueButton room on their server for the release party.

Expecting you all there!