---
title: "Debian Day’22, Bishop Heber College, Trichy"
date: 2022-08-24T18:30:00+05:30
draft: false
author: "Arun Mani J"
authorUrl: "https://arun-mani-j.gitlab.io"
tags: ["debian", "debian-day", "trichy"]
---

# Debian Day’22, Bishop Heber College, Trichy

Debian GNU/Linux, the universal operating system was officially released in 16
August 1993 by Ian Murdock. To celebrate the birth day of Debian, open source
community and enthusiasts gather together, which is known by the name Debian
Day. Anyone is free to organize the event both virtually and physically. The
main objective of the Day is celebration, so any strict rules and protocols are
relaxed.

The free and open source users of south India planned to conduct the event in
Kerala and Tamil Nadu. Due to unfortunate flood related issues in Kerala, the
event was done through online. In Tamil Nadu, we organized the Day in Bishop
Heber College of Trichy. From the entire making blueprint to bringing it in
action was done by the students of BHC led by Ravishanker. Ash and Abraham Raji
guided us in the process. I reached the spot with the help of Ravi on 16 August.

The event was to be simple but fun. Since it is organized in a college where it
is highly possible that many are not aware of what Debian or Linux is, we
shifted our focus from just celebrating Debian to celebrating by spreading
awareness. Or in the other words, we wanted students and the audience to know
what Debian is, about the free and open source software etc.

![Photo of Vice Principal of BHC presenting the welcome speech](/images/debday22-trichy/vp.jpg "Vice Principal of BHC presenting the welcome speech")

The Day with prayer song followed by welcome speech by the Vice Principal of
BHC. He was such a lively person whose mere presence itself meant a big support.
He said he spent a few hours last night on searching about Debian, `dpkg` etc.
He was very happy to know that the event is organized by students who are
involved in free and open source contributions during their studies itself. We
then joined with the Principal of BHC who met us virtually and welcomed us to
the event.

After his speech, Ravishanker presented a lightning talk on FOSS. He had a
stuffed up presentation with memes and points that was successful in engaging
the audience to the stage.

![Photo of Trishiraj on the day](/images/debday22-trichy/trishiraj.jpg "Trishiraj on the day")

Following that, Trishiraj, the lead developer of CorvusOS joined us via online.
Being a native of Tamil Nadu, he spoke in a mix of Tamil and English, which was
highly useful because the audience felt his words. Trishi talked about his ROM,
the Android Open Source Project and a layperson guide to contributions.

After this, I presented a speech on history of Debian to how to get started with
it. I also included my personal history on how I got familiar with both FOSS and
Debian.

![Photo of Kyle presenting his talk](/images/debday22-trichy/kyle.jpg "Kyle presenting his talk")

We had a small tea-break which was followed by a talk by Kyle Robbertze, Debian
Developer. Kyle joined us virtually and presented about the advantages of
Debian, the contribution guide and etc. Kyle’s talk brought a lot of questions
like aboutpackage management, Debian’s Salsa etc. Due to technical issues on our
side, I attended the QA for him. The final event was an interactive quiz on
FOSS. The questions were based on different spectrum of FOSS but from the user’s
perspective. The last question was a Beat the Clock, with the question of naming
as many as FOSS applications/projects as possible. We gave a small memento to
the student who named the majority.

![Photo of a few organizers of the event](/images/debday22-trichy/group-pic.jpg "A few organizers of the event, Ravi on the extreme right and me to his left")

The Day was awesome. It was the first event we conducted both on our own and
represented the Debian community (instead of students). The main target we set
for the event was that we need to kindle a spark in the minds of students and
staffs about Debian and FOSS. In that regard, the event is a grand success. We
had the chance to meet the Vice Principal after the event and he was very happy
for the Day. He was so inspired by Debian and the community that he said that
they will and be welcoming to conduct more such events.

The Day could not have been possible without the FOSS community and so, it is
neither _you_, nor _me_, it is _us_. We did it. Looking forward to join more
such events in the coming days!
